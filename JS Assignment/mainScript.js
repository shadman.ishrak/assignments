var data;
var entry;
var count = 0;
var serial = 0;
var tableHeading;
var tableData = "";
var globalindex = -500;
var base64String = "hello"
var ratingFlag = -1;
var updateFlag = false;
var parentRow;

document.addEventListener("DOMContentLoaded", function() {
    init();
    tableLoad();
})

function init() {
    data = new Array();
    ratingFlag = -1;
    updateFlag = false;
    showHint();
}

function formatData() {
    reset();
    ratingSort();
    showHint();
}

function tableLoad() {

    if (data.length == 0){
        document.getElementById("emptyMessage").style = "display: block";
        document.getElementById("table").style = "display: none";
    }
    else {
        let tableRef = document.getElementById("tableBody");
        tableRef.innerHTML = "";

        tableHeading = document.getElementById("ratingMarker").innerHTML;
        if (ratingFlag == -1) {
            document.getElementById("ratingMarker").innerHTML = tableHeading.replace("RATING⮝", "RATING⮟");
        } else {
            document.getElementById("ratingMarker").innerHTML = tableHeading.replace("RATING⮟", "RATING⮝");
        }

        for (var i = 0; i < data.length; i++) {
            if (data[i].flag == true) {
                var row = document.createElement('tr');
                tableData = "";
                row.setAttribute = ("index", data[i].uniqueID);
                var buttonText = '<div class="tableButtons"><button class="buttons update" type="button" index="DUMMY" onclick="updateEntry(this)">   Update   </button><button class="buttons delete" type = "button" index="DUMMY" onclick="removeEntry(this)"> Delete </button> </div>';
                tableData += "<td class='name'>" + data[i].name + "</td><td>" + data[i].address + "</td><td>" + data[i].rating + "</td><td class='land'>" + data[i].type + '</td><td><img src="data:image/jpeg;charset=utf-8;base64, ' + data[i].picture + '"></td><td>' + buttonText.split("DUMMY").join(data[i].uniqueID) + "</td>";
                row.innerHTML = tableData;
                tableRef.appendChild(row);
            }
        }

        
        document.getElementById("emptyMessage").style = "display: none";
        document.getElementById("table").style = "display: auto";
    }
}

function showHint() {
    if (data.length == 0) {
        document.getElementById("search").style.display = "none";
        tableLoad();
        return;
    } else {
        document.getElementById("search").style.display = "block";
    }

    var hint = document.getElementById("search").value.toLowerCase();
    if (hint.length == 0) {
        document.getElementById("search").innerHTML = "";
        for (var i = 0; i < data.length; i++) {
            data[i].flag = true;
        }
        tableLoad();
        return;
    } else {
        tableData = tableHeading;
        hint = hint.toLowerCase();
        for (var i = 0; i < data.length; i++) {
            if (hint == data[i].name.substr(0, hint.length)) {
                data[i].flag = true;
            } else data[i].flag = false;
        }
        tableLoad();
    }
}

function ratingChange() {
    ratingFlag *= -1;
    formatData();
}

function updateIndex() {
    for (var i = 0; i < data.length; i++) {
        data[i].uniqueID = i;
    }
}

function removeEntry(x) {
    var index = parseInt(x.getAttribute("index"));
    var deleteIndex;
    if (count == 1) {
        data = [];
        count--;
        showHint();
        return;
    }
    for (var i = 0; i < data.length; i++) {
        if (data[i].uniqueID == index) {
            deleteIndex = index;
        }
    }
    data.splice(deleteIndex, 1);
    count--;

    formatData();
}

function updateEntry(x) {
    if (updateFlag == true) {
        return;
    }
    updateIndex();
    var index = parseInt(x.getAttribute("index"));
    parentRow = (x.parentNode.parentNode.parentNode);
    parentRow.style = 'background-color: #CCE7E7';
    var entry;
    for (var i = 0; i < data.length; i++) {
        if (data[i].uniqueID == index) {
            entry = data[i];
            globalindex = i;
        }
    }
    document.getElementById("name").value = entry.name;
    document.getElementById("address").value = entry.address;
    document.getElementById("rating").value = entry.rating
    document.getElementById("type").value = entry.type;
    document.getElementById("picture").value = "";

    document.getElementById("oldImage").innerHTML = '<img src="data:image/jpeg;charset=utf-8;base64, ' + entry.picture + '">'
    updateFlag = true;
}

function compare(a, b) {
    const bandA = parseInt(a.rating);
    const bandB = parseInt(b.rating);

    let comparison = 0;
    if (bandA > bandB) {
        comparison = 1;
    } else if (bandA < bandB) {
        comparison = -1;
    }
    return comparison * ratingFlag;
}

function ratingSort() {
    data.sort(compare);
    updateIndex();
}


function reset() {
    document.getElementById("form").reset();
}

var getBase64OfFile = function(file, callback) {
    var fr = new FileReader();
    fr.addEventListener('loadend', (e) => {
        if (typeof callback === 'function') {
            callback(fr.result.split('base64,')[1]);
        }
    });
    fr.readAsDataURL(file);
}

function validateInput() {
    var validationFlag = true;
    var name = document.getElementById('name');
    var address = document.getElementById('address');
    var rating = document.getElementById('rating');
    var type = document.getElementById('type');
    var picture = document.getElementById('picture');

    if (name.value == "") {
        name.style = "border: 2px solid crimson";
        document.getElementById("nameWarning").style.display = "flex";
        validationFlag = false;
    } else {
        name.style = "";
        document.getElementById("nameWarning").style.display = "none";
    }

    if (address.value == "") {
        address.style = "border: 2px solid crimson";
        document.getElementById("addressWarning").style.display = "flex";
        validationFlag = false;
    } else {
        address.style = "";
        document.getElementById("addressWarning").style.display = "none";
    }

    if (rating.value < 1 || rating.value > 5) {
        rating.style = "border: 2px solid crimson";
        document.getElementById("ratingWarning").style.display = "flex";
        validationFlag = false;
    } else {
        rating.style = ""
        document.getElementById("ratingWarning").style.display = "none";
    }

    if (updateFlag == false) {
        if (picture.value == "") {
            picture.style = "border: 2px solid crimson";
            document.getElementById("pictureWarning").style.display = "flex";
            validationFlag = false;
        } else {
            picture.style = ""
            document.getElementById("pictureWarning").style.display = "none";
        }
    }


    if (validationFlag == false)
        return false;

    return true;
}

function insert() {

    if (validateInput() == false) {
        return;
    }

    entry = new Object();
    entry.name = document.getElementById("name").value.toLowerCase();
    entry.address = document.getElementById("address").value;
    entry.rating = document.getElementById("rating").value;
    entry.type = document.getElementById("type").value;
    entry.picture = document.getElementById("picture").value;

    if (updateFlag == true && entry.picture == "") {
        entry.picture = data[globalindex].picture;
        entry.uniqueID = data.length;
        count++;
        entry.flag = true;
        if (globalindex == -500)
            data.push(entry);
        else {
            data.splice(globalindex, 1, entry);
            globalindex = -500;
        }
        updateFlag = false;
        parentRow.style = "";
        formatData();
    } else {
        var file = document.getElementById('picture').files[0];
        getBase64OfFile(file, (data2) => {
            entry.picture = data2;

            entry.uniqueID = data.length;
            count++;
            entry.flag = true;


            if (globalindex == -500)
                data.push(entry);
            else {
                data.splice(globalindex, 1, entry);
                globalindex = -500;
            }
            updateFlag = false;
            document.getElementById("oldImage").innerHTML = "";
            formatData();
        });
    }
}
