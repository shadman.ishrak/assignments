﻿using Assignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment.Data
{
    public static class SeedData
    {
        public static void Initialize(Database context)
        {
            context.City.AddRange(
                new City("Dhaka"),
                new City("Barisal"),
                new City("Chittagong")
            );
            context.Cinema.AddRange(
                new Cinema
                {
                    Name = "Bashundhara",
                    CityId = 1
                },
                new Cinema
                {
                    Name = "Millenium",
                    CityId = 2
                },
                new Cinema
                {
                    Name = "Sanmar",
                    CityId = 3
                }
            );

            context.Movie.AddRange(
                new Movie
                {
                    Name = "Harry Potter",
                    ReleaseDate = new DateTime(2020, 10, 1),
                    Rating = 4.8,
                },
                new Movie
                {
                    Name = "Contagion",
                    ReleaseDate = new DateTime(2011, 10, 1),
                    Rating = 4.5,
                },
                new Movie
                {
                    Name = "The Expanse",
                    ReleaseDate = new DateTime(2015, 10, 1),
                    Rating = 3.5,
                },
                new Movie
                {
                    Name = "Dragon Ball",
                    ReleaseDate = new DateTime(2020, 10, 1),
                    Rating = 3.2,
                },
                new Movie
                {
                    Name = "Pirates of the Carribean",
                    ReleaseDate = new DateTime(2012, 10, 1),
                    Rating = 4.0,
                }

            );

            context.Weekdays.AddRange(
                new Weekdays { Day = "Sunday" },
                new Weekdays { Day = "Monday" },
                new Weekdays { Day = "Tuesday" },
                new Weekdays { Day = "Wednesday" },
                new Weekdays { Day = "Thursday" },
                new Weekdays { Day = "Friday" },
                new Weekdays { Day = "Saturday" }

            );

            context.Hall.AddRange(
                new Hall("d Hall1", 1, 50),
                new Hall("d Hall2", 1, 25),
                new Hall("b hall1", 2, 25),
                new Hall("c hall1", 3, 25)
            );

            context.Schedule.AddRange(
                new Schedule(1, "Sunday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(1, "Monday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(1, "Tuesday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(1, "Wednesday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(1, "Thursday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(1, "Friday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(1, "Sunday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(1, "Monday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(1, "Tuesday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(1, "Wednesday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(1, "Thursday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(1, "Friday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),

                new Schedule(2, "Sunday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(2, "Monday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(2, "Tuesday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(2, "Wednesday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(2, "Thursday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(2, "Friday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(2, "Sunday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(2, "Monday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(2, "Tuesday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(2, "Wednesday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(2, "Thursday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(2, "Friday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),

                new Schedule(3, "Sunday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(3, "Monday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(3, "Tuesday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(3, "Wednesday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(3, "Thursday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(3, "Saturday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(3, "Sunday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(3, "Monday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(3, "Tuesday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(3, "Wednesday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(3, "Thursday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(3, "Saturday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),

                new Schedule(4, "Sunday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(4, "Monday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(4, "Tuesday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(4, "Saturday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(4, "Thursday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(4, "Friday", DateTime.Parse("12:00"), DateTime.Parse("16:00")),
                new Schedule(4, "Sunday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(4, "Monday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(4, "Tuesday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(4, "Saturday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(4, "Thursday", DateTime.Parse("16:00"), DateTime.Parse("20:00")),
                new Schedule(4, "Friday", DateTime.Parse("16:00"), DateTime.Parse("20:00"))
            );

            context.MovieSchedule.AddRange(
                new MovieSchedule(4, 19, DateTime.Parse("11/08/2019"), 350),
                new MovieSchedule(5, 10, DateTime.Parse("11/07/2019"), 400),
                new MovieSchedule(5, 28, DateTime.Parse("01/16/2020"), 450),
                new MovieSchedule(3, 28, DateTime.Parse("10/17/2019"), 450),
                new MovieSchedule(1, 3, DateTime.Parse("10/15/2019"), 350),
                new MovieSchedule(5, 26, DateTime.Parse("12/08/2019"), 400),
                new MovieSchedule(5, 7, DateTime.Parse("03/18/2020"), 350),
                new MovieSchedule(1, 43, DateTime.Parse("07/26/2019"), 450),
                new MovieSchedule(4, 35, DateTime.Parse("01/11/2020"), 400),
                new MovieSchedule(2, 20, DateTime.Parse("01/05/2020"), 300),
                new MovieSchedule(2, 2, DateTime.Parse("05/13/2019"), 400),
                new MovieSchedule(1, 46, DateTime.Parse("09/08/2019"), 450),
                new MovieSchedule(5, 41, DateTime.Parse("08/17/2019"), 450),
                new MovieSchedule(3, 10, DateTime.Parse("08/12/2019"), 450),
                new MovieSchedule(1, 13, DateTime.Parse("05/29/2019"), 450),
                new MovieSchedule(5, 19, DateTime.Parse("09/10/2019"), 450),
                new MovieSchedule(2, 7, DateTime.Parse("04/11/2020"), 350),
                new MovieSchedule(1, 9, DateTime.Parse("12/02/2019"), 350),
                new MovieSchedule(2, 23, DateTime.Parse("02/06/2020"), 350),
                new MovieSchedule(5, 26, DateTime.Parse("12/30/2019"), 300),
                new MovieSchedule(3, 25, DateTime.Parse("11/04/2019"), 450),
                new MovieSchedule(2, 27, DateTime.Parse("08/07/2019"), 350),
                new MovieSchedule(3, 26, DateTime.Parse("07/24/2019"), 450),
                new MovieSchedule(5, 18, DateTime.Parse("06/28/2019"), 400),
                new MovieSchedule(1, 15, DateTime.Parse("12/01/2019"), 300),
                new MovieSchedule(2, 21, DateTime.Parse("10/23/2019"), 450),
                new MovieSchedule(3, 38, DateTime.Parse("08/30/2019"), 300),
                new MovieSchedule(5, 40, DateTime.Parse("08/09/2019"), 450),
                new MovieSchedule(2, 23, DateTime.Parse("02/18/2020"), 400),
                new MovieSchedule(1, 35, DateTime.Parse("05/01/2019"), 400),
                new MovieSchedule(3, 36, DateTime.Parse("08/07/2019"), 300),
                new MovieSchedule(5, 18, DateTime.Parse("05/28/2019"), 400),
                new MovieSchedule(4, 26, DateTime.Parse("02/20/2020"), 300),
                new MovieSchedule(4, 43, DateTime.Parse("06/12/2019"), 350),
                new MovieSchedule(3, 13, DateTime.Parse("06/03/2019"), 350),
                new MovieSchedule(2, 46, DateTime.Parse("04/15/2020"), 300),
                new MovieSchedule(3, 13, DateTime.Parse("10/22/2019"), 400),
                new MovieSchedule(1, 47, DateTime.Parse("12/14/2019"), 300),
                new MovieSchedule(2, 24, DateTime.Parse("10/02/2019"), 350),
                new MovieSchedule(3, 38, DateTime.Parse("10/27/2019"), 300)

            );

            context.User.AddRange(
                 new User("Adam", "Levine", "M", DateTime.Parse("10/27/1980"), 1),
                 new User("Michael", "Jackson", "M", DateTime.Parse("10/27/1970"), 1)
            );

            context.Booking.AddRange(
                    new Booking(2, 1, 1),
                    new Booking(1, 1, 1)
                );

            context.SaveChanges();
        }
    }
}