﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment.Models;

namespace Assignment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeekdaysController : ControllerBase
    {
        private readonly Database _context;

        public WeekdaysController(Database context)
        {
            _context = context;
        }

        // GET: api/Weekdays
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Weekdays>>> GetWeekdays()
        {
            return await _context.Weekdays.ToListAsync();
        }

        // GET: api/Weekdays/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Weekdays>> GetWeekdays(int id)
        {
            var weekdays = await _context.Weekdays.FindAsync(id);

            if (weekdays == null)
            {
                return NotFound();
            }

            return weekdays;
        }

        [Route("day/{days}")]
        public async Task<ActionResult<Weekdays>> GetWeekdays(string days)
        {
            var weekdays = await _context.Weekdays.ToListAsync();
            var day = weekdays.Single(w => w.Day.ToLower() == days.ToLower());

            return day;
        }

        // PUT: api/Weekdays/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWeekdays(int id, Weekdays weekdays)
        {
            if (id != weekdays.Id)
            {
                return BadRequest();
            }

            _context.Entry(weekdays).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WeekdaysExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Weekdays
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Weekdays>> PostWeekdays(Weekdays weekdays)
        {
            _context.Weekdays.Add(weekdays);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetWeekdays", new { id = weekdays.Id }, weekdays);
        }

        // DELETE: api/Weekdays/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Weekdays>> DeleteWeekdays(int id)
        {
            var weekdays = await _context.Weekdays.FindAsync(id);
            if (weekdays == null)
            {
                return NotFound();
            }

            _context.Weekdays.Remove(weekdays);
            await _context.SaveChangesAsync();

            return weekdays;
        }

        private bool WeekdaysExists(int id)
        {
            return _context.Weekdays.Any(e => e.Id == id);
        }
    }
}