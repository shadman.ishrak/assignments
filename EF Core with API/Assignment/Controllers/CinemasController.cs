﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment.Models;
using System.Net.Http;
using Newtonsoft.Json;

namespace Assignment.Controllers
{
    public class Result
    {
        public Result(string cinema, string movie)
        {
            Cinema = cinema;
            Movie = movie;
        }

        public string Cinema { get; set; }
        public string Movie { get; set; }
    }

    public class Query2
    {
        public String Name { get; set; }
        public int Count { get; set; }

        public Query2(string name, int count)
        {
            Name = name;
            Count = count;
        }
    }

    public class Query3
    {
        public string HallName { get; set; }
        public int ScheduleId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }

        public string Day { get; set; }

        public Query3(string hallName, int sCheduleId, string startTime, string endTime, string day)
        {
            HallName = hallName;
            ScheduleId = sCheduleId;
            StartTime = startTime;
            EndTime = endTime;
            Day = day;
        }
    }

    [Route("api/[controller]")]
    [ApiController]
    public class CinemasController : ControllerBase
    {
        private readonly Database _context;

        public CinemasController(Database context)
        {
            _context = context;
        }

        // GET: api/Cinemas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Cinema>>> GetCinema()
        {
            return await _context.Cinema.ToListAsync();
        }

        // GET: api/Cinemas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Cinema>> GetCinema(int id)
        {
            var cinema = await _context.Cinema.FindAsync(id);

            if (cinema == null)
            {
                return NotFound();
            }

            return cinema;
        }

        // PUT: api/Cinemas/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCinema(int id, Cinema cinema)
        {
            if (id != cinema.CinemaId)
            {
                return BadRequest();
            }

            _context.Entry(cinema).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CinemaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Cinemas
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Cinema>> PostCinema(Cinema cinema)
        {
            _context.Cinema.Add(cinema);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCinema", new { id = cinema.CinemaId }, cinema);
        }

        // DELETE: api/Cinemas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Cinema>> DeleteCinema(int id)
        {
            var cinema = await _context.Cinema.FindAsync(id);
            if (cinema == null)
            {
                return NotFound();
            }

            _context.Cinema.Remove(cinema);
            await _context.SaveChangesAsync();

            return cinema;
        }

        private bool CinemaExists(int id)
        {
            return _context.Cinema.Any(e => e.CinemaId == id);
        }

        //api/cinemas/query1
        [HttpGet("query1")]
        public List<Result> query1()
        {
            var query = from cinema in _context.Set<Cinema>()

                        join hall in _context.Set<Hall>()
                        on cinema.CinemaId equals hall.CinemaId
                        join schedule in _context.Set<Schedule>()
                        on hall.HallId equals schedule.HallId
                        join movieSchedule in _context.Set<MovieSchedule>()
                        on schedule.ScheduleId equals movieSchedule.ScheduleId
                        join movie in _context.Set<Movie>()
                        on movieSchedule.MovieId equals movie.MovieId
                        select new { cinema, movie };

            List<Result> results = new List<Result>();
            List<Result> distinctResults = new List<Result>();

            foreach (var q in query)
            {
                results.Add(new Result(q.cinema.Name, q.movie.Name));
            }

            var cinemas = _context.Cinema;
            foreach (var cinema in cinemas)
            {
                distinctResults.AddRange(
                      results.Where(item => item.Cinema == cinema.Name).GroupBy(item => item.Movie).Select(groups => groups.First()).ToList()
                );
            }

            return distinctResults;
        }

        //api/cinemas/query2
        [HttpGet("query2")]
        public List<Query2> query2()
        {
            List<Query2> results = new List<Query2>();
            var query = from cinema in _context.Set<Cinema>()
                        join hall in _context.Set<Hall>()
                        on cinema.CinemaId equals hall.CinemaId
                        join schedule in _context.Set<Schedule>()
                        on hall.HallId equals schedule.HallId
                        join movieSchedule in _context.Set<MovieSchedule>()
                        on schedule.ScheduleId equals movieSchedule.ScheduleId
                        join movie in _context.Set<Movie>().Where(movie => movie.ReleaseDate >= DateTime.Parse("2020-01-01"))
                        on movieSchedule.MovieId equals movie.MovieId
                        group cinema by cinema.Name into g
                        select new
                        {
                            g.Key,
                            Count = g.Count()
                        };

            foreach (var item in query)
            {
                results.Add(
                    new Query2(item.Key, item.Count));
            }

            return results;
        }

        //api/cinemas/query3
        [HttpGet("query3")]
        public List<Query3> query3()
        {
            List<Query3> results = new List<Query3>();

            var allSchedules = from hall in _context.Set<Hall>()
                               join schedule in _context.Set<Schedule>()
                               on hall.HallId equals schedule.HallId
                               select new
                               {
                                   hall, schedule
                               };

            var schedulesWithMovies = from hall in _context.Set<Hall>()
                                      join schedule in _context.Set<Schedule>()
                                      on hall.HallId equals schedule.HallId
                                      join movieSchedule in _context.Set<MovieSchedule>()
                                      on schedule.ScheduleId equals movieSchedule.ScheduleId
                                      select new
                                      {
                                          hall,
                                          schedule
                                      };

            var schedulesWithoutMovies = allSchedules.Except(schedulesWithMovies);

            foreach (var item in schedulesWithMovies)
            {
                results.Add(
                        new Query3(item.hall.Name, item.schedule.ScheduleId, item.schedule.StartTime.ToString("HH:mm"), item.schedule.EndTime.ToString("HH:mm"), item.schedule.Day));
            }

            return results;
        }

        //api/cinemas/query4/1
        [HttpGet("query4/{hallid}")]
        public List<string> query4(int hallid)
        {
            var weekDays = from weekdays in _context.Set<Weekdays>()
                           select new { weekdays.Day };

            var daysOpen = from hall in _context.Set<Hall>()
                           join schedule in _context.Set<Schedule>().Where(hall => hall.HallId == hallid)
                           on hall.HallId equals schedule.HallId
                           select new { schedule.Day };

            var weekEnd = weekDays.Except(daysOpen);

            List<string> result = new List<string>();
            foreach (var item in weekEnd)
            {
                result.Add(item.Day);
            }

            return result;
        }

        //api/cinemas/query5?userId=1&tickets=2&movieScheduleId=1
        [HttpGet("query5")]
        public int query5(int userId, int tickets, int movieScheduleId)
        {
            if (tickets > 4) return -2;

            var query = from user in _context.Set<User>().Where(user => user.UserId == userId)
                        join booking in _context.Set<Booking>()
                        on user.UserId equals booking.UserId
                        select new
                        {
                            user.UserId,
                            booking.NumberOfSeats
                        };

            int totalTickets = 0;
            foreach (var item in query)
            {
                totalTickets += item.NumberOfSeats;
            }

            if (tickets + totalTickets > 4) return -2;

            var nquery = from hall in _context.Set<Hall>()
                         join schedule in _context.Set<Schedule>()
                         on hall.HallId equals schedule.HallId
                         join movieSchedule in _context.Set<MovieSchedule>().Where(i => i.MovieScheduleId == movieScheduleId)
                         on schedule.ScheduleId equals movieSchedule.ScheduleId
                         join nbooking in _context.Set<Booking>()
                         on movieSchedule.MovieScheduleId equals nbooking.MovieScheduleId
                         select new
                         {
                             hall.Capacity, nbooking.BookingId, nbooking.NumberOfSeats
                         };

            int alreadyBookedSeats = 0;
            int hallCapacity = 0;
            foreach (var item in nquery)
            {
                hallCapacity = item.Capacity;
                alreadyBookedSeats += item.NumberOfSeats;
            }

            if (alreadyBookedSeats + tickets > hallCapacity) return -1;

            Booking newBooking = new Booking(tickets, movieScheduleId, userId);
            _context.Booking.Add(newBooking);
            _context.SaveChanges();

            var ticketPrice = from movieSchedule in _context.Set<MovieSchedule>().Where(x => x.MovieScheduleId == movieScheduleId)
                              select new { movieSchedule.TicketPrice };

            int price = 0;
            foreach (var item in ticketPrice)
            {
                price += item.TicketPrice;
            }
            return price * tickets;
        }
    }
}