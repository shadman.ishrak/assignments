﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment.Models;

namespace Assignment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieSchedulesController : ControllerBase
    {
        private readonly Database _context;

        public MovieSchedulesController(Database context)
        {
            _context = context;
        }

        // GET: api/MovieSchedules
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieSchedule>>> GetMovieSchedule()
        {
            return await _context.MovieSchedule.ToListAsync();
        }

        // GET: api/MovieSchedules/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieSchedule>> GetMovieSchedule(int id)
        {
            var movieSchedule = await _context.MovieSchedule.FindAsync(id);

            if (movieSchedule == null)
            {
                return NotFound();
            }

            return movieSchedule;
        }

        // PUT: api/MovieSchedules/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovieSchedule(int id, MovieSchedule movieSchedule)
        {
            if (id != movieSchedule.MovieScheduleId)
            {
                return BadRequest();
            }

            _context.Entry(movieSchedule).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieScheduleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MovieSchedules
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MovieSchedule>> PostMovieSchedule(MovieSchedule movieSchedule)
        {
            _context.MovieSchedule.Add(movieSchedule);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovieSchedule", new { id = movieSchedule.MovieScheduleId }, movieSchedule);
        }

        // DELETE: api/MovieSchedules/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieSchedule>> DeleteMovieSchedule(int id)
        {
            var movieSchedule = await _context.MovieSchedule.FindAsync(id);
            if (movieSchedule == null)
            {
                return NotFound();
            }

            _context.MovieSchedule.Remove(movieSchedule);
            await _context.SaveChangesAsync();

            return movieSchedule;
        }

        private bool MovieScheduleExists(int id)
        {
            return _context.MovieSchedule.Any(e => e.MovieScheduleId == id);
        }
    }
}
