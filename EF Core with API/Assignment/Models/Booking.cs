﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Assignment.Models
{
    public class Booking

    {
        public Booking(int numberOfSeats, int movieScheduleId, int userId)
        {
            NumberOfSeats = numberOfSeats;
            MovieScheduleId = movieScheduleId;
            UserId = userId;
        }

        public int BookingId { get; set; }

        [Required]
        public int NumberOfSeats { get; set; }

        [Required]
        public int MovieScheduleId { get; set; }

        [JsonIgnore]
        public MovieSchedule MovieSchedule { get; set; }

        [Required]
        public int UserId { get; set; }

        [JsonIgnore]
        public User User { get; set; }
    }
}