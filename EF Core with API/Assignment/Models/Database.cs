﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assignment.Models;

namespace Assignment.Models
{
    public class Database : DbContext
    {
        public Database(DbContextOptions<Database> options)
            : base(options)
        {
        }

        public DbSet<Assignment.Models.Movie> Movie { get; set; }

        public DbSet<Assignment.Models.City> City { get; set; }

        public DbSet<Assignment.Models.Cinema> Cinema { get; set; }

        public DbSet<Assignment.Models.User> User { get; set; }

        public DbSet<Assignment.Models.Booking> Booking { get; set; }

        public DbSet<Assignment.Models.Hall> Hall { get; set; }

        public DbSet<Assignment.Models.MovieSchedule> MovieSchedule { get; set; }

        public DbSet<Assignment.Models.Schedule> Schedule { get; set; }

        public DbSet<Assignment.Models.Weekdays> Weekdays { get; set; }
    }
}