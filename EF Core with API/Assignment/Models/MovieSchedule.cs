﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Assignment.Models
{
    public class MovieSchedule
    {
        public int MovieScheduleId { get; set; }

        public MovieSchedule(int movieId, int scheduleId, DateTime date, int ticketPrice)
        {
            MovieId = movieId;
            ScheduleId = scheduleId;
            Date = date;
            TicketPrice = ticketPrice;
        }

        [Required]
        public int TicketPrice { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public int MovieId { get; set; }

        [JsonIgnore]
        public Movie Movie { get; set; }

        public int ScheduleId { get; set; }

        [JsonIgnore]
        public Schedule Schedule { get; set; }

        public ICollection<Booking> Bookings { get; set; }
    }
}