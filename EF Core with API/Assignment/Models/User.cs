﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Assignment.Models
{
    public class User
    {
        public User(string firstName, string lastName, string gender, DateTime dateOfBirth, int cityId)
        {
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
            DateOfBirth = dateOfBirth;
            CityId = cityId;
        }

        public int UserId { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Gender { get; set; }

        public DateTime DateOfBirth { get; set; }

        [Required]
        public int CityId { get; set; }

        [JsonIgnore]
        public City City { get; set; }

        public ICollection<Booking> Bookings { get; set; }
    }
}