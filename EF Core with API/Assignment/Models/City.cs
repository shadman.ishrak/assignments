﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment.Models
{
    public class City
    {
        public City(string name)
        {
            Name = name;
        }

        public int CityId { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<Cinema> Cinemas { get; set; }
        public ICollection<User> Users { get; set; }
    }
}