﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Assignment.Models
{
    public class Schedule
    {
        public Schedule(int hallId, string day, DateTime startTime, DateTime endTime)
        {
            HallId = hallId;
            Day = day;
            StartTime = startTime;
            EndTime = endTime;
        }

        public int ScheduleId { get; set; }

        [Required]
        public DateTime StartTime { get; set; }

        [Required]
        public DateTime EndTime { get; set; }

        [Required]
        public string Day { get; set; }

        [Required]
        public int HallId { get; set; }

        [JsonIgnore]
        public Hall Hall { get; set; }

        public ICollection<MovieSchedule> MovieSchedules { get; set; }
    }
}