﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Assignment.Models
{
    public class Hall
    {
        public Hall(string name, int cinemaId, int capacity)
        {
            Name = name;
            Capacity = capacity;
            CinemaId = cinemaId;
        }

        public int HallId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int Capacity { get; set; }

        [Required]
        public int CinemaId { get; set; }

        [JsonIgnore]
        public Cinema Cinema { get; set; }

        public ICollection<Schedule> Schedules { get; set; }
    }
}