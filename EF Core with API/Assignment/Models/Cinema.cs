﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Assignment.Models
{
    public class Cinema
    {
        public int CinemaId { get; set; }

        [Required]
        public string Name { get; set; }

        public int CityId { get; set; }

        [JsonIgnore]
        public City City { get; set; }

        public ICollection<Hall> Halls { get; set; }
    }
}