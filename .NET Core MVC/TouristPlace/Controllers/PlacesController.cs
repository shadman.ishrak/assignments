﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TouristPlace.Data;
using TouristPlace.Models;

namespace TouristPlace.Controllers {

    public class PlacesController : Controller {
        private bool sorted = false;
        private readonly MvcPlaceContext _context;
        private readonly IWebHostEnvironment _iwebhost;

        private void debug(string message) {
            System.Diagnostics.Debug.WriteLine("debug: " + message);
        }

        private List<string> supportedFileTypes = new List<string>()
        {
            ".jpeg",
            ".jpg",
            ".gif",
            ".png",
        };

        public PlacesController(MvcPlaceContext context, IWebHostEnvironment iwebhost) {
            _context = context;
            _iwebhost = iwebhost;
        }

        public void Sort() {
            if (sorted)
                sorted = false;
            else {
                sorted = true;
            }
        }

        // GET: Places
        public ActionResult Index(string searchString, string sortOrder) {
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["RatingSortParm"] = sortOrder == "Rating" ? "rating_desc" : "Rating";

            var places = from m in _context.Place
                         select m;

            switch (sortOrder) {
                case "name_desc":
                    places = places.OrderByDescending(s => s.Name);
                    break;

                case "Rating":
                    places = places.OrderBy(s => s.Rating);
                    break;

                case "rating_desc":
                    places = places.OrderByDescending(s => s.Rating);
                    break;

                default:
                    places = places.OrderBy(s => s.Name);
                    break;
            }

            if (!String.IsNullOrEmpty(searchString)) {
                places = places.Where(s => s.Name.Contains(searchString));
                ViewData["searchTerms"] = "Showing results for '" + searchString + "'";
            }
            else {
                ViewData["searchTerms"] = "";
            }
            return View(places);
        }

        public async Task<IActionResult> Details(int? id) {
            if (id == null) {
                return NotFound();
            }

            var place = await _context.Place
                .FirstOrDefaultAsync(m => m.Id == id);
            if (place == null) {
                return NotFound();
            }

            return View(place);
        }

        public IActionResult Create() {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Address,Rating")] Place place, IFormFile Picture) {
            if (Picture == null) {
                ViewData["imageMessage"] = "The Image field is required.";
                return View(place);
            }

            string imgExtension = Path.GetExtension(Picture.FileName);

            if (!supportedFileTypes.Contains(imgExtension)) {
                ViewData["imageMessage"] = "Unsupported file type.";
                return View(place);
            }

            string filename = Guid.NewGuid() + imgExtension;

            var savedImage = Path.Combine(_iwebhost.WebRootPath, "images", filename);
            var stream = new FileStream(savedImage, FileMode.Create);
            await Picture.CopyToAsync(stream);
            place.Picture = filename;

            if (ModelState.IsValid) {
                _context.Add(place);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(place);
        }

        public async Task<IActionResult> Edit(int? id) {
            if (id == null) {
                return NotFound();
            }

            var place = await _context.Place.FindAsync(id);
            if (place == null) {
                return NotFound();
            }
            return View(place);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Address,Rating")] Place place, IFormFile Picture) {
            if (id != place.Id) {
                return NotFound();
            };
            var oldValue = await _context.Place.AsNoTracking().FirstOrDefaultAsync(a => a.Id == id);

            if (Picture == null) {
                place.Picture = oldValue.Picture;
            }
            else {
                string imgExtension = Path.GetExtension(Picture.FileName);

                if (!supportedFileTypes.Contains(imgExtension)) {
                    ViewData["imageMessage"] = "Unsupported file type.";
                    return View(place);
                }
                string filename = Guid.NewGuid() + imgExtension;
                var savedImage = Path.Combine(_iwebhost.WebRootPath, "images", filename);
                var stream = new FileStream(savedImage, FileMode.Create);
                await Picture.CopyToAsync(stream);
                place.Picture = filename;
            }

            if (ModelState.IsValid) {
                try {
                    _context.Update(place);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException) {
                    if (!PlaceExists(place.Id)) {
                        return NotFound();
                    }
                    else {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(place);
        }

        public async Task<IActionResult> Delete(int? id) {
            if (id == null) {
                return NotFound();
            }

            var place = await _context.Place
                .FirstOrDefaultAsync(m => m.Id == id);
            if (place == null) {
                return NotFound();
            }

            return View(place);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id) {
            var place = await _context.Place.FindAsync(id);
            _context.Place.Remove(place);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlaceExists(int id) {
            return _context.Place.Any(e => e.Id == id);
        }

        [HttpGet]
        public ActionResult Search() {
            return PartialView("_SearchFormPartial");
        }
    }
}