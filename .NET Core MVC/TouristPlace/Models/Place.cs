﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TouristPlace.Models {

    public class Place {
        public int Id { get; set; }

        [StringLength(60)]
        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        [Range(1, 5)]
        [Required]
        public decimal Rating { get; set; }

        public byte[] Image { get; set; }

        public string Picture { get; set; }
    }
}