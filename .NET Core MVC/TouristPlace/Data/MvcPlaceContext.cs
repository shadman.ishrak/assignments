﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TouristPlace.Models;

namespace TouristPlace.Data {

    public class MvcPlaceContext : DbContext {

        public MvcPlaceContext(DbContextOptions<MvcPlaceContext> options)
            : base(options) {
        }

        public DbSet<Place> Place { get; set; }
    }
}