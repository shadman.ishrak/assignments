var data;
var entry;
var count = 0;
var serial = 0;
var tableHeading;
var tableData = "";
var globalindex = -500;
var base64String = "hello"
var ratingFlag = -1;
var updateFlag = false;
var parentRow;


$(document).ready(function(){
    assignEventListeners();
    init();
    tableLoad();
});


function assignEventListeners() {
    $("#search").keyup(function (e) { 
       showHint(); 
    });
    $("#ratingMarker").click(function (e) { 
        ratingChange();
    });
    $("#submit").click(function (e) { 
       insert();
    });
    $("#reset").click(function (e) { 
        reset();
    });
    $(document).on("click", ".delete" , function(e) {
        removeEntry($(this));
    });
    $(document).on("click", ".update" , function(e) {
        updateEntry($(this));
    });
}

function init() {
    data = new Array();
    ratingFlag = -1;
    updateFlag = false;
    showHint();
}

function formatData() {
    reset();
    ratingSort();
    showHint();
}

function tableLoad() {

    if (data.length == 0){
        // $("#emptyMessage").css("display", "auto");
        $("#emptyMessage").show();
        $("#table").hide();
    }
    else {
        $("#tableBody").html("");
        tableHeading = $("#ratingMarker").html();
        if (ratingFlag == -1) {
            $("#ratingMarker").html(tableHeading.replace("RATING⮝", "RATING⮟"));
        } else {
            $("#ratingMarker").html(tableHeading.replace("RATING⮟", "RATING⮝"));
        }

        for (var i = 0; i < data.length; i++) {
            if (data[i].flag == true) {
                var row = document.createElement("tr");
                tableData = "";
                row.setAttribute = ("index", data[i].uniqueID);
                var buttonText = '<div class="tableButtons"><button class="buttons update" type="button" index="DUMMY">   Update   </button><button class="buttons delete" type = "button" index="DUMMY"> Delete </button> </div>';
                tableData = "<td class='name'>" + data[i].name + "</td><td>" + data[i].address + "</td><td>" + data[i].rating + "</td><td class='land'>" + data[i].type + '</td><td><img src="data:image/jpeg;charset=utf-8;base64, ' + data[i].picture + '"></td><td>' + buttonText.split("DUMMY").join(data[i].uniqueID) + "</td>";
                row.innerHTML = tableData;
                $("#tableBody").append(row);
                
            }
            
        }

        
        $("#emptyMessage").hide();
        //todo
        // $("#table").css("display", "auto");
        // document.getElementById("table").style = "display: auto";
        $("#table").show();
    }
}

function showHint() {
    if (data.length == 0) {
        $("#search").css("display", "none");
        tableLoad();
        return;
    } else {
        $("#search").css("display", "block");
    }

    var hint = $("#search").val().toLowerCase();
    if (hint.length == 0) {
        $("#search").html("");
        for (var i = 0; i < data.length; i++) {
            data[i].flag = true;
        }
        tableLoad();
        return;
    } else {
        tableData = tableHeading;
        hint = hint.toLowerCase();
        for (var i = 0; i < data.length; i++) {
            if (hint == data[i].name.substr(0, hint.length)) {
                data[i].flag = true;
            } else data[i].flag = false;
        }
        tableLoad();
    }
}

function ratingChange() {
    ratingFlag *= -1;
    formatData();
}

function updateIndex() {
    for (var i = 0; i < data.length; i++) {
        data[i].uniqueID = i;
    }
}

function removeEntry(x) {
    var index = parseInt(x.attr("index"));
    var deleteIndex;
    if (count == 1) {
        data = [];
        count--;
        showHint();
        return;
    }
    for (var i = 0; i < data.length; i++) {
        if (data[i].uniqueID == index) {
            deleteIndex = index;
        }
    }
    data.splice(deleteIndex, 1);
    count--;

    formatData();
}

function updateEntry(x) {
    if (updateFlag == true) {
        return;
    }
    updateIndex();

    var index = parseInt(x.attr("index"));
    parentRow = (x.parent().parent().parent());
    parentRow.css('background-color', '#CCE7E7');
    var entry;
    for (var i = 0; i < data.length; i++) {
        if (data[i].uniqueID == index) {
            entry = data[i];
            globalindex = i;
        }
    }
    $("#name").val(entry.name);
    $("#address").val(entry.address);
    $("#rating").val(entry.rating);
    $("#type").val(entry.type);
    $("#picture").val("");
    $("#oldImage").html('<img src="data:image/jpeg;charset=utf-8;base64, ' + entry.picture + '">');
    updateFlag = true;
}

function compare(a, b) {
    const bandA = parseInt(a.rating);
    const bandB = parseInt(b.rating);

    let comparison = 0;
    if (bandA > bandB) {
        comparison = 1;
    } else if (bandA < bandB) {
        comparison = -1;
    }
    return comparison * ratingFlag;
}

function ratingSort() {
    data.sort(compare);
    updateIndex();
}


function reset() {
    $("#name").val("");
    $("#address").val("");
    $("#rating").val("1");
    $("#type").val("Beach");
    $("#pciture").val("");

}

var getBase64OfFile = function(file, callback) {
    var fr = new FileReader();
    fr.addEventListener('loadend', (e) => {
        if (typeof callback === 'function') {
            callback(fr.result.split('base64,')[1]);
        }
    });
    fr.readAsDataURL(file);
}

function validateInput() {
    var validationFlag = true;
    var name = $('#name');
    var address = $('#address');
    var rating = $('#rating');
    var picture = $('#picture');

    if (name.val() == "") {
        name.css("border", "2px solid crimson");
        $("#nameWarning").css("display", "flex");
        validationFlag = false;
    } else {
        name.css("border", "1px solid teal");
        $("#nameWarning").css("display", "none");
    }

    if (address.val() == "") {
        address.css("border", "2px solid crimson");
        $("#addressWarning").css("display", "flex");
        validationFlag = false;
    } else {
        address.css("border", "1px solid teal");
        $("#addressWarning").css("display", "none");
    }

    if (rating.val() < 1 || rating.val() > 5) {
        rating.css("border", "2px solid crimson");
        $("#ratingWarning").css("display", "flex");
        validationFlag = false;
    } else {
        rating.css("border", "1px solid teal");
        $("#ratingWarning").css("display", "none");
    }

    if (updateFlag == false) {
        if (picture.val() == "") {
            picture.css("border", "2px solid crimson");
            $("#pictureWarning").css("display", "flex");
            validationFlag = false;
        } else {
            picture.css("border", "1px solid teal");
            $("#pictureWarning").css("display", "none");
        }
    }


    if (validationFlag == false)
        return false;

    return true;
}

function insert() {

    if (validateInput() == false) {
        return;
    }

    entry = new Object();
    entry.name = $("#name").val().toLowerCase();
    entry.address = $("#address").val();
    entry.rating = $("#rating").val();
    entry.type = $("#type").val();
    entry.picture = $("#picture").val();

    if (updateFlag == true && entry.picture == "") {
        entry.picture = data[globalindex].picture;
        entry.uniqueID = data.length;
        count++;
        entry.flag = true;
        if (globalindex == -500)
            data.push(entry);
        else {
            data.splice(globalindex, 1, entry);
            globalindex = -500;
        }
        updateFlag = false;
        parentRow.style = "";
        formatData();
    } else {
        var file = $('#picture').prop('files')[0];
        getBase64OfFile(file, (data2) => {
            entry.picture = data2;

            entry.uniqueID = data.length;
            count++;
            entry.flag = true;


            if (globalindex == -500)
                data.push(entry);
            else {
                data.splice(globalindex, 1, entry);
                globalindex = -500;
            }
            updateFlag = false;
            formatData();
        });
    }
    $("#oldImage").html("");
}
