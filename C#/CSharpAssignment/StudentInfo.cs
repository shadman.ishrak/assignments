﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpAssignment {

    public class StudentInfo {
        public String Name { get; set; }
        public int Roll { get; set; }
    }
}