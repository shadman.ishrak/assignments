﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Extensions.Configuration;

namespace CSharpAssignment {

    internal class Solver {
        private string outputPath, studentInfoPath, studentResultPath;
        private StreamReader studentResult;
        private StreamReader studentInfo;
        private List<StudentResult> listOfStudents = new List<StudentResult>();
        private List<StudentResult> sortedListOfStudents;

        public Solver() {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appSettings.json");
            var configuration = builder.Build();

            outputPath = configuration["outputPath"];
            studentInfoPath = configuration["studentInfoPath"];
            studentResultPath = configuration["studentResultPath"];
            studentInfo = new StreamReader(studentInfoPath);
            studentResult = new StreamReader(studentResultPath);
        }

        public void populateStudents() {
            string line;
            while ((line = studentInfo.ReadLine()) != null) {
                var data = line.Split(',');
                StudentResult student = new StudentResult();
                student.Name = data[0];
                student.Roll = int.Parse(data[1]);
                listOfStudents.Add(student);
            }
        }

        public void populateResults() {
            string line;
            while ((line = studentResult.ReadLine()) != null) {
                var data = line.Split(',');
                var index = listOfStudents.FindIndex(student => student.Roll == int.Parse(data[0]));

                switch (data[1]) {
                    case "MATH":
                        listOfStudents[index].Math = int.Parse(data[2]);
                        break;

                    case "PHY":
                        listOfStudents[index].Phy = int.Parse(data[2]);
                        break;

                    case "CHEM":
                        listOfStudents[index].Chem = int.Parse(data[2]);
                        break;
                }
            }
        }

        public void sort() {
            sortedListOfStudents = listOfStudents
                .OrderByDescending(student => student.calculateGrade())
                .ThenBy(student => student.Roll)
                .ThenBy(student => student.Name).ToList();
        }

        public void generateResults() {
            string header = "ROLL\tNAME\tCG\n----\t----\t--";
            Console.WriteLine(header);
            File.WriteAllText(outputPath, header + "\n");
            foreach (var student in sortedListOfStudents) {
                string content = (
                    student.Roll +
                    "\t" + student.Name +
                    "\t" + (double)System.Math.Round(student.cg, 2));
                File.AppendAllText(outputPath, content + "\n");
                Console.WriteLine(content);
            }
        }
    }
}