﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpAssignment {

    public class StudentResult : StudentInfo {
        public int Math { get; set; } = 0;
        public int Phy { get; set; } = 0;
        public int Chem { get; set; } = 0;

        public double cg;

        public double calculateGrade() {
            cg = (double)(Math + Phy + Chem) / 3;
            return cg;
        }
    }
}