﻿using System;

namespace CSharpAssignment {

    public class Program {

        private static void Main(string[] args) {
            Solver solver = new Solver();
            solver.populateStudents();
            solver.populateResults();
            solver.sort();
            solver.generateResults();

            Console.ReadKey();
        }
    }
}