function load() {
  var xhttp, xmlDoc, temp, x, y;
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {

        var jsonData = this.responseText;
        var data = JSON.parse(jsonData);
        var table = "<table><tr>";

        for (x in data[0]) {
            table += "<th>" + x + "</th>";
        }
        table += "</tr>";

         for (i = 0; i < data.length; i++) {
            table += "<tr>";
            for (y in data[i]) {
                table += "<td>" + data[i][y] + "</td>";
            }
            table += "</tr>"
         }

        document.getElementById("t1").innerHTML = table;
    }
    else {
        document.getElementById("t1").innerHTML = "ERROR";
    }
  };
  xhttp.open("GET", "https://jsonplaceholder.typicode.com/posts", true);
  xhttp.send();

  document.getElementById("btn1").innerHTML = "REFRESH DATA";

  // document.getElementById("t1").innerHTML = "ERROR";
}
